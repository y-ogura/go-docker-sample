# build and run
build:
	go build
	./go-docker-sample

# docker build
docker-build:
	docker buid -t registry.gitlab.com/y-ogura/go-docker-sample:latest .

# docker push
docker-push:
	docker push registry.gitlab.com/y-ogura/go-docker-sample

# run run docker
run:
	docker run -p 1323:1323 -td 1e68d0e75841
