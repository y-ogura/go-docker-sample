package hello

import (
	"net/http"

	"github.com/labstack/echo"
)

// Controller hello world
type Controller struct{}

// New mount hello controller
func New(e *echo.Echo) {
	handler := &Controller{}

	e.GET("/", handler.Hello)
}

// Hello say hello
func (c *Controller) Hello(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, "Hello World!")
}
