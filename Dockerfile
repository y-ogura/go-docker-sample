# Build
FROM golang:1.11.1-stretch AS build
ADD . /go/src/gitlab.com/y-ogura/go-docker-sample
WORKDIR /go/src/gitlab.com/y-ogura/go-docker-sample
ENV GO111MODULE on
RUN go build -o app

# New copy it into our base image.
FROM gcr.io/distroless/base
COPY --from=build /go/src/gitlab.com/y-ogura/go-docker-sample/app /
ENV PORT 1323
EXPOSE 1323
CMD ["/app"]
