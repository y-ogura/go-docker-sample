package main

import (
	"os"

	"github.com/labstack/echo"
	"gitlab.com/y-ogura/go-docker-sample/hello"
)

func main() {
	e := echo.New()

	hello.New(e)

	port := ":" + os.Getenv("PORT")
	e.Logger.Fatal(e.Start(port))
}
